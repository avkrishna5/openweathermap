import { Config, browser } from "protractor";
import { Reporter } from "../support/reporter";

const jsonReports = process.cwd() + "/reports/json";

export const config: Config = {

  directConnect: true,
  
//   seleniumAddress: "http://localhost:4444/wd/hub",

  SELENIUM_PROMISE_MANAGER: false, //using async and await

  // capabilities: {
  //       'browserName': 'chrome',
  //       'chromeOptions': {
  //         'args': [
  //           "--no-sandbox",
  //           "--disable-dev-shm-usage"]
  //       }
  //     },

  capabilities: {
  'browserName': 'chrome',
  'chromeOptions': {
    'args': ['no-sandbox', 'headless', 'disable-gpu']
  }
},

   specs: [
        "../../features/Home.feature",
    ],

    framework: "custom",
    frameworkPath: require.resolve("protractor-cucumber-framework"),

    params: {
           baseUrl: "https://openweathermap.org/",
    },

    onPrepare: () => {
        browser.ignoreSynchronization = true;
        browser.manage().window().maximize();
        Reporter.createDirectory(jsonReports);
    },
    
    cucumberOpts: {
        compiler: "ts:ts-node/register",
        format: "json:./reports/json/cucumber_report.json",
        require: ["../../typeScript/stepdefinitions/*.js", "../../typeScript/support/*.js"],
        strict: true,
        tags: "@smoke",
    },

    onComplete: () => {
        Reporter.createHTMLReport();
    },

}