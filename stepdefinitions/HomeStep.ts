
import { HomePageObject } from "../pages/HomePage";
import {Given, When, Then} from "cucumber";

const objHome: HomePageObject = new HomePageObject();

Given(/^I am on the home page$/, async () => {
    await objHome.verifyHomeTitle();
});

Given(/^I navigate to home page$/, async () => {
    await objHome.gotoHome();
});

Then(/^I verify city search text field$/, async () => {
    await objHome.verifyCitySearchTextField();
});

Then(/^I verify city search button$/, async () => {
    await objHome.verifyCitySearchButton();
});

Then(/^I verify degrees and fahrenheit options$/, async () => {
    await objHome.verifyTempUnits();
});

Then(/^I verify signin link$/, async () => {
    await objHome.verifySigninLink();
});

When(/^I search by "(.*?)" city$/, async (text) => {
    await objHome.searchByCity(text);
});

Then(/^I see city not found messages$/, async () => {
    await objHome.verifyCityNotFoundMessage();
});

Then(/^I see the weather details of the desired city$/, async () => {
    await objHome.verifyCityWeatherDetails();
});

When(/^I select "(.*?)" tab$/, async (text) => {
    await objHome.clickWidgetTab(text);
});

Then(/^I see "(.*?)" in the widget$/, async (text) => {
    await objHome.verifyTabWidget(text);
});