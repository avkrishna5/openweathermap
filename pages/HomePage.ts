import { element, by, browser, WebElement } from "protractor";
import {AllPages}  from "../pages/AllPages";

const allPages : AllPages = new AllPages();
const chai = require("chai").use(require("chai-as-promised"));
const expect = chai.expect;

export class HomePageObject {
    
    public txtCitySearch: WebElement;
    public btnCitySearch: WebElement;
    public spnDegrees;spnFarenheit: WebElement;
    public lnkSignin: WebElement;
    public objNotFoundAlert;objWeatherDetails;objWeatherWidget: WebElement;
    public tabMain;tabDaily;tabHourly;tabChart;tabMap: WebElement;
    public widMain;widDaily;widHourly;widChart;widMap: WebElement;
    public strExpectedHomeTitle: String;

    constructor(){
//*[@id="weather-widget"]/div[1]/div/div/div[2]/div[2]
        this.txtCitySearch=element(by.xpath('//*[@id="weather-widget"]/div[1]/div/div/div[2]/div[1]/div/input'));
        this.btnCitySearch=element(by.xpath('//*[@id="weather-widget"]/div[1]/div/div/div[2]/div[1]/button'));
        this.spnDegrees=element(by.id('metric'));
        this.spnFarenheit=element(by.id('imperial'));
        this.lnkSignin=element(by.id('sign_in'));
        this.objNotFoundAlert=element(by.xpath('//*[@id="weather-widget"]/div[1]/div/div/div[2]/div[2]'));
        this.objWeatherDetails=element(by.xpath('//*[@id="forecast_list_ul"]/table'));
        this.objWeatherWidget=element(by.xpath('//*[@id="weather-widget"]/h3'));
        this.tabMain=element(by.id('tab-main'));
        this.tabDaily=element(by.id('tab-daily'));;
        this.tabHourly=element(by.id('tab-hourly'));
        this.tabChart=element(by.id('tab-chart'));
        this.tabMap=element(by.id('tab-map'));
        this.widMain=element(by.className('weather-forecast-hourly-graphic__header'));
        this.widDaily=element(by.className('weather-forecast-graphic__header'));
        this.widHourly=element(by.className('weather-forecast-hourly-list__header'));
        this.widChart=element(by.className('weather-forecast-chartval-graphic__header'));
        this.widMap=element(by.className('weather-map-layers__header'));
        this.strExpectedHomeTitle = "Сurrent weather and forecast - OpenWeatherMap";
    }

    async gotoHome(){
        await allPages.btnHome.click();
    }

    async verifyHomeTitle(){
        await allPages.verifyPageTitle(this.strExpectedHomeTitle);
    }

    async verifyCitySearchTextField(){
        await this.txtCitySearch.isDisplayed();
    }

    async verifyCitySearchButton(){
        await this.btnCitySearch.isDisplayed();
    }

    async verifyTempUnits(){
        await this.spnDegrees.isDisplayed();
        await this.spnFarenheit.isDisplayed();
    }

    async veirfyInvalidSearch(){
          browser.pause()
          await this.objNotFoundAlert.isDisplayed();
    }

    async verifyValidSearch(){
        await this.objWeatherDetails.isDisplayed();
    }

    async verifySigninLink(){
        await this.lnkSignin.isDisplayed();
    }

    async searchByCity(strCity: string){
        await this.txtCitySearch.sendKeys(strCity);
        await this.btnCitySearch.click();
    }

    async verifyCityNotFoundMessage(){
        await this.objNotFoundAlert.isDisplayed();
    }

    async verifyCityWeatherDetails(){
        await this.objWeatherDetails.isDisplayed();
    }

    async clickWidgetTab(strTabName: string){
        switch(strTabName){
            case "Main":{
                this.tabMain.click();
                break;
            }
            case "Daily":{
                this.tabDaily.click();
                break;
            }
            case "Hourly":{
                this.tabHourly.click();
                break;
            }
            case "Chart":{
                this.tabChart.click();
                break;
            }
            case "Map":{
                this.tabMap.click();
                break;
            }
        }
    }

    async verifyTabWidget(strTabName: string){
        switch(strTabName){
            case "Main":{
                expect(this.widMain.isDisplayed()).to.eventually.equals(true);
                break;
            }
            case "Daily":{
                expect(this.widDaily.isDisplayed()).to.eventually.equals(true);
                break;
            }
            case "Hourly":{
                expect(this.widHourly.isDisplayed()).to.eventually.equals(true);
                break;
            }
            case "Chart":{
                expect(this.widChart.isDisplayed()).to.eventually.equals(true);
                break;
            }
            case "Map":{
                expect(this.widMap.isDisplayed()).to.eventually.equals(true);
                break;
            }
        }
    }
}