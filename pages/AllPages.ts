import { browser, WebElement,element,by } from "protractor";

const chai = require("chai").use(require("chai-as-promised"));
const expect = chai.expect;

export class AllPages {

    public btnHome: WebElement;

    constructor(){
        this.btnHome=element(by.xpath('/html/body/ul/li[1]/a/img'));
    }

    async getPageTitle() : Promise<String> {
        let pageTitle = "";
        browser.getTitle().then(function(titleText) {
            pageTitle = titleText;
        });
        return pageTitle;
    }

    async verifyPageTitle(pageTitle : String) {
        await expect(browser.getTitle()).to.eventually.equal(pageTitle);
    };
  

};
