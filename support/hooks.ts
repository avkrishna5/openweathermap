import { browser } from "protractor";

const { BeforeAll, After, AfterAll, Status, setDefaultTimeout } = require("cucumber");

setDefaultTimeout(120000);

BeforeAll(async () => {
    await browser.get(browser.params.baseUrl);
  });

After(async function()  {
    const screenShot = await browser.takeScreenshot();
    this.attach(screenShot, "image/png");
});

AfterAll({timeout: 100 * 1000}, async () => {
  await browser.quit();
});
