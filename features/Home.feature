Feature: OpenWeatherMap application

@smoke
Scenario: Verify basic details
    Given I am on the home page
    Then I verify city search text field
    And I verify city search button
    And I verify degrees and fahrenheit options
    And I verify signin link

@smoke
Scenario Outline: Try searching the weather details by entering invalid city name
    Given I am on the home page
    When I search by "<cityName>" city
    Then I see city not found messages
Examples:
| cityName | 
| 123city123  |

# @smoke
# Scenario Outline: Try searching the weather details by entering valid city name
#     Given I navigate to home page
#     Then I am on the home page
#     When I search by "<cityName>" city
#     Then I see the weather details of the desired city
# Examples:
# | cityName | 
# | Bengaluru, IN |

# @smoke
# Scenario Outline: Verify all tabs in the widget
#     Given I navigate to home page
#     Then I am on the home page
#     When I select "<tabName>" tab
#     Then I see "<tabName>" in the widget
# Examples:
# | tabName | 
# | Main | 
# | Daily | 
# | Hourly | 
# | Chart | 
# | Map |

