**Overview:**

**Framework: Protractor-Cucumber-TypeScript**

**Framework features:**
- Approach - Behavior Driven Development (BDD)
- Design Pattern – Page Object Model
- Test Reports
    -  Html reports alongside JSON (using “cucumber-html-reporter” lib)
    - Screenshots for both pass and fail scenarios
- Hooks – to support BeforeAll, After and AfterAll actions
- Test Configuration
    - tag-based execution (cucumberOpts) 
    - user can set default timeout in the hooks file
    - async/await – readable and clean coding 
    - using ChromeDriver directly (directConnect: true)
- TypeScript: Strongly typed- helps in easy development & maintenance of the code
- Continuous Integration – GitLab - tests run on a dockerhub image for every push to the master.

**Usage:**

**Pre-requisites:**
- Node.js  v8.12.0 or higher
- JDK -v8
- Visual Studio Code v1.33.0 or higher
- Gitlab Account
- Chrome browser installed (using chromedriver directly)

**Getting Started:**

**Local-Execution:**
1. Create a new local folder (say MyFramework)  and navigate to this path, then clone the repository 
    - _git clone https://gitlab.com/avkrishna5/openweathermap.git_
2. Launch terminal/command prompt/VS Code and navigate to MyFramework > openweathermap and install all dependencies 
    - _npm install_
3. Ensure there is a newly created node_modules sub folder in your project folder
4. Downloading browsers driver binaries
    - _npm run webdriver-update_
    - _webdriver-manager update --proxy <proxyurl:port>_ (use this just in case behind proxy)
5. Starting selenium server (Optional step – as we are using chromedriver directly)
    - _npm run webdriver-start_
6. Transpiling all the required .ts files to .js files using below command
    - _npm run clean-build_
7. Ensure there is a newly created typeScript sub folder in your project folder     
8. Run the scripts on chrome browser 
    - _npm test_
9. Once the run is completed, you may look for html reports in reports folder

**GitLab-CI:** 
1.	Launch the project in VS Code and make any changes to the code – probably the easiest way is to change the test data in feature file.
2.	In the Source Control tab, stage the modified file and commit with proper comments
3.	Push the code to git
    - _git push_
4.	Navigate to the gitlab url and go to CI/CD > Jobs
5.	You will see a job with name dependencies running – click on it to see the console
6.	Once the dependencies job is passed, another job with name test starts running - click on it to see the console
7.	Ideally if your changes are valid, the recent commit gets tagged as passed.
8.	In case of failed jobs, an email with brief log will be triggered to the job owners.

**Quick Facts:**
1.	The setup uses ‘cucumber-html-reporter’ lib which converts JSON reports to HTML reports and the user has the option to select the
    desired  report theme.
    - _https://www.npmjs.com/package/cucumber-html-reporter_
2.	Gitlab CI - using the below docker container to run javascript tests in a headless machine
    - _docker pull weboaks/node-karma-protractor-chrome_
3.	If you are behind a proxy, make sure you give the desired proxy settings wherever required
